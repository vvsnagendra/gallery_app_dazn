package com.example.galleryapp.data

import android.content.Context
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types

class ImagesRepositoryImpl(private val context: Context) : ImagesRepository {


    override suspend fun getImages(): List<ImageResponse>? {

         //Created a new listype because moshi.apapter dosen't take directly the List<ImageResponse> as a parameter

        val listType = Types.newParameterizedType(List::class.java, ImageResponse::class.java)
        val adapter: JsonAdapter<List<ImageResponse>> = getMoshiInstance().adapter(listType)

        val imageJson = context.assets.open("nasa_details.json").bufferedReader().use { it.readText() }
        return adapter.fromJson(imageJson)
    }



      //Creating a static object to get the instance of the moshi builder
    companion object MoshiFactory {
        private val moshi: Moshi? = null

        // Singleton moshi initialization
        fun getMoshiInstance(): Moshi {
            return moshi ?: Moshi.Builder().build()
        }
    }
}