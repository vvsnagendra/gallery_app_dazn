package com.example.galleryapp.data


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ImageResponse(
    @Json(name = "copyright")
    var copyright: String? = null,
    @Json(name = "date")
    var date: String? = null,
    @Json(name = "explanation")
    var explanation: String? = null,
    @Json(name = "hdurl")
    var hdurl: String? = null,
    @Json(name = "media_type")
    var mediaType: String? = null,
    @Json(name = "service_version")
    var serviceVersion: String? = null,
    @Json(name = "title")
    var title: String? = null,
    @Json(name = "url")
    var url: String? = null
)