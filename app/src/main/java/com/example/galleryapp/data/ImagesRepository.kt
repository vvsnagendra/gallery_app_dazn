package com.example.galleryapp.data

interface ImagesRepository{
    suspend fun getImages(): List<ImageResponse>?
}