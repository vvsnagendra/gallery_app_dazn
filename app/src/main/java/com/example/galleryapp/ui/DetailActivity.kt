package com.example.galleryapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.example.galleryapp.data.ImagesRepositoryImpl
import com.example.galleryapp.databinding.ActivityDetailBinding

class DetailActivity : AppCompatActivity() {


    private lateinit var mBinding: ActivityDetailBinding


    private val detailsAdapter: DetailsAdapter by lazy {
        DetailsAdapter()
    }

    private val clickedPosition: Int by lazy {
        intent.getIntExtra("position", 0)
    }

    private lateinit var imagesViewModel: ImagesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        //instantiating view model
        imagesViewModel = ViewModelProvider(this, ImagesViewModelFactory(ImagesRepositoryImpl(this)))[ImagesViewModel::class.java]

        loadDataAndUpdateUI()
    }

    private fun loadDataAndUpdateUI() {
        //Get the data from the viewModel
        imagesViewModel.getImageData()

        mBinding.vpDetailScreen.adapter = detailsAdapter
//        mBinding.vpDetailScreen.setPageTransformer(ViewPagerTransformation())


        //Observing the data and applying the appropriate data condition
        imagesViewModel.imagesLiveData.observe(this) {
            it?.let {
                detailsAdapter.swapData(it)
                //Set smooth scroll to false so that it doesn't animate when selected at certain position
                mBinding.vpDetailScreen.setCurrentItem(clickedPosition, false)
            }
        }
    }
}