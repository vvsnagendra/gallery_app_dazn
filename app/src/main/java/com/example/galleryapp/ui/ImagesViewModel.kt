package com.example.galleryapp.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.galleryapp.data.ImagesRepository
import com.example.galleryapp.data.ImageResponse
import kotlinx.coroutines.launch

class ImagesViewModel(private val imagesRepository: ImagesRepository) : ViewModel() {

    //Here creating mutable version to pass the data
    private val _imagesLiveData : MutableLiveData<List<ImageResponse>> = MutableLiveData()
    //To observe that data we use this
    val imagesLiveData : LiveData<List<ImageResponse>> = _imagesLiveData


    fun getImageData() {
        viewModelScope.launch {
            _imagesLiveData.postValue(imagesRepository.getImages())
        }
    }

}