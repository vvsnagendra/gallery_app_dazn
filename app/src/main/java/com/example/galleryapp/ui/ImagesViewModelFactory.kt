package com.example.galleryapp.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.galleryapp.data.ImagesRepository

/**
 * Passing an parameter in ViewModel requires you to create the ViewModelFactory class
 * to override the Base Implementation of the Android ViewmodelFactory without parameter*/
class ImagesViewModelFactory(private val imagesRepository: ImagesRepository) :
    ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return ImagesViewModel(
            imagesRepository
        ) as T
    }
}