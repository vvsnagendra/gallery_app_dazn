package com.example.galleryapp.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.galleryapp.data.ImagesRepositoryImpl
import com.example.galleryapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityMainBinding

    private val imagesListAdapter: ImagesListAdapter by lazy {
        ImagesListAdapter()
    }

    private lateinit var imagesViewModel: ImagesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        //Getting the instance of the View model
        imagesViewModel = ViewModelProvider(this, ImagesViewModelFactory(ImagesRepositoryImpl(this)))[ImagesViewModel::class.java]

        //Get the data from the viewModel
        imagesViewModel.getImageData()

        mBinding.recyclerview.adapter = imagesListAdapter


        //Observing the data and applying the appropriate data condition
        imagesViewModel.imagesLiveData.observe(this) {
            it?.let {
                imagesListAdapter.swapData(it)
            }
        }

        setUpListener()


    }

    private fun setUpListener() {
        imagesListAdapter.itemClickListener = this::handleItemClick
    }

    private fun handleItemClick(position: Int) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra("position", position)
        startActivity(intent)
    }


}