package com.example.galleryapp.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.example.galleryapp.R
import com.example.galleryapp.data.ImageResponse

class DetailsAdapter :
    ListAdapter<ImageResponse, DetailsAdapter.ImageViewHolder>(ImageResponseDC()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ImageViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.item_description, parent, false)
    )

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    fun swapData(data: List<ImageResponse>?) {
        submitList(data?.toMutableList())
    }

    inner class ImageViewHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: ImageResponse) = with(itemView) {
            val tvTitle = itemView.findViewById<TextView>(R.id.tvTitle)
            val tvDate = itemView.findViewById<TextView>(R.id.tvDate)
            val tvCopyright = itemView.findViewById<TextView>(R.id.tvCopyright)
            val tvDetails = itemView.findViewById<TextView>(R.id.tvDetails)
            val ivImage = itemView.findViewById<ImageView>(R.id.ivImage)
            tvTitle.text = item.title
            tvDate.text = item.date
            //Handle null check to make the view gone
            item.copyright?.let {
                tvCopyright.text = it
            } ?: kotlin.run { tvCopyright.visibility = View.GONE }
            tvDetails.text = item.explanation
            Glide.with(this).load(item.hdurl).placeholder(CircularProgressDrawable(itemView.context).apply {
                strokeWidth = 10f
                centerRadius = 80f
                start()
            })
                .into(ivImage)
        }
    }

    private class ImageResponseDC : DiffUtil.ItemCallback<ImageResponse>() {
        override fun areItemsTheSame(
            oldItem: ImageResponse,
            newItem: ImageResponse
        ): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(
            oldItem: ImageResponse,
            newItem: ImageResponse
        ): Boolean {
            return oldItem == newItem
        }
    }

}
