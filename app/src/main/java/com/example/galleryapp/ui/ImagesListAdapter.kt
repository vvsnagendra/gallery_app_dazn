package com.example.galleryapp.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.example.galleryapp.R
import com.example.galleryapp.data.ImageResponse

class ImagesListAdapter :
    ListAdapter<ImageResponse, ImagesListAdapter.ImageViewHolder>(ImageResponseDC()) {

    //Setup the Item ClickListener to pass the click position
    var itemClickListener: (position: Int) -> Unit = {}


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        return ImageViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_img, parent, false)
        )
    }


    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    fun swapData(data: List<ImageResponse>?) {
        submitList(data?.toMutableList())
    }

    inner class ImageViewHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {

            if (adapterPosition == RecyclerView.NO_POSITION) return
            itemClickListener.invoke(adapterPosition)
        }

        fun bind(item: ImageResponse) = with(itemView) {
            val tvTitle = itemView.findViewById<TextView>(R.id.tvTitle)
            val ivImage = itemView.findViewById<ImageView>(R.id.ivImage)
            tvTitle.text = item.title
            Glide.with(this).load(item.url)
                .placeholder(CircularProgressDrawable(itemView.context).apply {
                    strokeWidth = 10f
                    centerRadius = 50f
                    start()
                }).into(ivImage)
        }
    }

    private class ImageResponseDC : DiffUtil.ItemCallback<ImageResponse>() {
        override fun areItemsTheSame(
            oldItem: ImageResponse,
            newItem: ImageResponse
        ): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(
            oldItem: ImageResponse,
            newItem: ImageResponse
        ): Boolean {
            return oldItem == newItem
        }
    }

}